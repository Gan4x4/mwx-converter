from unittest import TestCase
import main
from main import Node

class TestStartSmoke(TestCase):
    def test_start_smoke(self):
        input_string = "repeat 2 [fd 4]"
        expected = [Node('for (let i=2; i>0; i--){\n\tyield* prims.fd(4);\n\n}')]
        actual = main.start(input_string)

        self.assertEquals(len(expected), len(actual))
        for i in range(0, len(actual)):
            e = expected[i].value.strip(' \t\n\r')
            a = actual[i].value.strip(' \t\n\r')
            self.assertEquals(e, a)

    def test_sf1(self):
        input_string = """if 3 = 3 [if 2 = 2 [tto "t1]]"""
        expected = [Node("""if ((3==3)) {\n\tif ((2==2)) {\n\tyield* prims.tto(\"t1\");\n\n};\n\n}""")]
        actual = main.start(input_string)

        self.assertEquals(len(expected), len(actual))
        for i in range(0, len(actual)):
            e = expected[i].value.strip(' \t\n\r')
            a = actual[i].value.strip(' \t\n\r')
            self.assertEquals(e, a)


