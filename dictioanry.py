__author__ = 'duckoteka'

dict = {
    'make': {
        'class': 1,
        'args': 2,
        'trans': 'scope.make',
    },
    'pick': {
        'class': 0,
        'args': 1,
        'trans': 'prims.pick',
    },
    'random': {
        'class': 0,
        'args': 1,
        'trans': 'prims.random',
    },
    'power': {
        'class': 0,
        'args': 2,
    },
    'abs': {
        'class': 0,
        'args': 1,
        'trans': 'prims.abs',
    },
    'sum': {
        'class': 0,
        'args': 2,
        'trans': 'prims.sum',
    },
    'ascii': {
        'class': 0,
        'args': 1,
        'trans': 'prims.ascii',
    },
    'arctan': {
        'class': 0,
        'args': 1,
        'tarns': 'prims.arctan',
    },
    'char': {
        'class': 0,
        'args': 1,
        'trans': 'prims.char'
    },
    'log': {
        'class': 0,
        'args': 1,
        'trans': 'prims.log'
    },
    'fd': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.fd',
    },
    'forward': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.fd',
    },
    'rt': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.rt',
    },
    'lt': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.lt',
    },
    'pd': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.pd()',
    },
    'pu': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.pu()',
    },
    'if': {
        'class': 1, #???
        'args': 2,
        'trans': 'if',
    },
    'forever': {
        'class': 1, #???
        'args': 1,
        'trans': '''\tprims.forever(function* (globals,processContext, obj){
                        var prims = processContext.prims;
                        var vars = globals.vars;
                        var userproc = globals.userproc;
                        var scope = vars.globalScope;\n\n''',
    },
    'repeat': {
        'class': 1,
        'args': 2,
        'trans': 'for (let i=',
    },
    'ifelse': {
        'class': 1,
        'args': 3,
        'trans': 'if',
    },
    'tto': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.tto(',
    },
    'setshape': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.setshape',
    },
    'setsh': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.setshape',
    },
    'setbg': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.setbg',
    },
    'setpensize': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.setpensize',
    },
    'shape': {
        'class': 0,
        'args': 0,
        'trans': 'prims.shape()',
    },
    'home': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.home()',
    },
    'clean': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.clean()',
    },
    'stopall': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.stopall()',
    },
    'stopme': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.stopme()',
    },
    'bk': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.bk',
    },
    'color': {
        'class': 0,
        'args': 0,
        'trans': 'prims.color()',
    },
    'size': {
        'class': 0,
        'args': 0,
        'trans': 'prims.size()',
    },
    'setcolor': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.setcolor',
    },
    'setc': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.setcolor',
    },
    'setsize': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.setsize',
    },
    'op': {
        'class': 1,
        'args': 1,
        'trans': 'return yield',
    },
    'wait': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.wait',
    },
    'announce': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.announce',
    },
    'getpage': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.getpage',
    },
    'clickon': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.clickon()',
    },
    'show': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.show',
    },
    'setheading': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.setheading',
    },
    'seth': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.setheading',
    },
    'heading': {
        'class': 0,
        'args': 0,
        'trans': 'prims.heading()',
    },
    'clickoff': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.clickoff()',
    },
    'colorunder': {
        'class': 0,
        'args': 0,
        'trans': 'prims.colorunder()',
    },
    'setpos': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.setpos',
    },
    'say': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.say',
    },
    'sayas': {
        'class': 1,
        'args': 2,
        'trans': 'yield* prims.sayas',
    },
    'everyone': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.everyone(function*(){',
    },
    'textwho': {
        'class': 0,
        'args': 0,
        'trans': 'prims.textwho()',
    },
    'hidetext': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.hidetext()',
    },
    'cleartext': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.cleartext()',
    },
    'ct': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.cleartext()',
    },
    'print': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.print',
    },
    'pr': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.print',
    },
    'insert': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.insert',
    },
    'showtext': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.showtext()',
    },
    'question': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.question',
    },
    'answer': {
        'class': 0,
        'args': 0,
        'trans': 'prims.answer()',
    },
    'sentence': {
        'class': 0,
        'args': 2,
        'trans': 'prims.sentence'
    },
    'se': {
        'class': 0,
        'args': 2,
        'trans': 'prims.sentence'
    },
    'st': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.st()'
    },
    'ht': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.ht()'
    },
    'setx': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.setx'
    },
    'sety': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.sety'
    },
    'resett': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.resett()'
    },
    'timer': {
        'class': 0,
        'args': 0,
        'trans': 'prims.timer()'
    },

    ###### NEW ->


    'bg': {
        'class': 0,
        'args': 0,
        'trans': 'prims.bg()'
    },
    'bottom': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.bottom()'
    },
    'butfirst': {
        'class': 0,
        'args': 1,
        'trans': 'prims.butfirst'
    },
    'butlast': {
        'class': 0,
        'args': 1,
        'trans': 'prims.butlast'
    },
    'cc': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.cc()'
    },
    'cb': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.cb()'
    },
    'cd': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.cd()'
    },
    'cf': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.cf()'
    },
    'cg': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.cg()'
    },
    'clearname': {
        'class': 1,
        'args': 1,
        'trans': 'prims.clearname'
    },
    'clearnames': {
        'class': 1,
        'args': 0,
        'trans': 'prims.clearnames()'
    },
    'cos': {
        'class': 0,
        'args': 1,
        'trans': 'prims.cos',
    },
    'createprojectvar': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.createprojectvar',
    },
    'cu': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.cu()',
    },
    'delete': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.delete()',
    },
    'distance': {
        'class': 0,
        'args': 1,
        'trans': 'prims.distance',
    },
    'empty?': {
        'class': 0,
        'args': 1,
        'trans': 'prims.empty',
    },
    'eol': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.eol()'
    },
    'eot?':{
        'class': 0,
        'args': 0,
        'trans': 'prims.eot()',
    },
    'errormessage': {
        'class': 0,
        'args': 0,
        'trans': 'prims.errormessage()',
    },
    'exp': {
        'class': 0,
        'args': 1,
        'trans': 'prims.exp'
    },
    'fill': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.fill()'
    },
    'first': {
        'class': 0,
        'args': 1,
        'trans': 'prims.first'
    },
    'fput': {
        'class': 0,
        'args': 2,
        'trans': 'prims.fput',
    },
    'freeze': {
        'class': 1,
        'args': 1,
        'trans': 'yield* prims.freeze',
    },
    'freezebg': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.freezebg()',
    },
    'glide': {
        'class': 1,
        'args': 2,
        'trans': 'yield* prims.glide'
    },
    'infront': {
        'class': 1,
        'args': 0,
        'trans': 'yield* prims.infront()'
    },
    'int': {
        'class': 0,
        'args': 1,
        'trans': 'prims.int'
    },
    'item': {
        'class': 0,
        'args': 2,
        'trans': 'prims.item'
    },
    'last': {
        'class': 0,
        'args': 1,
        'trans': 'prims.last'
    },
    'list': {
        'class': 0,
        'args': 2,
        'trans': 'prims.list'
    },
    'list?': {
        'class': 0,
        'args': 1,
        'trans': 'prims.islist'
    },
    'ln': {
        'class': 0,
        'args': 1,
        'trans': 'prims.ln'
    },
    'lput': {
        'class': 0,
        'args': 2,
        'trans': 'prims.lput'
    },
    'member?': {
        'class': 0,
        'args': 2,
        'trans': 'prims.member'
    },
    'minus': {
        'class': 0,
        'args': 1,
        'trans': 'prims.minus'
    },
    'count':{
        'class': 0,
        'args': 1,
        'trans': 'prims.count',
    },
    'top': {
        'class': 1,
        'args': 0,
        'trans': 'prims.top()'
    },
    'ycor': {
        'class': 0,
        'args': 0,
        'trans': 'prims.ycor()'
    },
    'select': {
        'class': 1,
        'args': 0,
        'trans': 'prims.select()'
    },
    'selected': {
        'class': 0,
        'args': 0,
        'trans': 'prims.selected()'
    },
    'copy': {
        'class': 1,
        'args': 0,
        'trans': 'prims.copy()'
    },
    'paste': {
        'class': 1,
        'args': 0,
        'trans': 'prims.paste()'
    },
    'xcor': {
        'class': 0,
        'args': 0,
        'trans': 'prims.xcor()'
    },
    'pos': {
        'class': 0,
        'args': 0,
        'trans': 'prims.pos()'
    },

}

infs = {
    '>': {
        'priority': 0,
        'trans': '>'
    },
    '<': {
        'priority': 0,
        'trans': '<'
    },
    '+': {
        'priority': 1,
        'trans': '+'
    },
    '-': {
        'priority': 1,
        'trans': '-'
    },
    '*': {
        'priority': 2,
        'trans': '*'
    },
    '/': {
        'priority': 2,
        'trans': '/'
    },
    '=': {
        'priority': 0,
        'trans': '=='
    },
}