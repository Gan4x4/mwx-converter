#! /usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Ivan Lukhovtsev'

from functools import reduce
import re
from dictioanry import dict, infs
import sys

class Node(object):
    def __init__(self, value):
        self.value = value
        self.children = []

    def add_child(self, obj):
        self.children.append(obj)

    def __repr__(self, level=0):
        ret = "\t"*level+repr(self.value)+"\n"
        for child in self.children:
            if child is not None and not isinstance(child, list):
                ret += child.__repr__(level+1)
            elif isinstance(child, list):
                ret += "\t"*3 + child.__repr__()
        return ret

    def isLeafHead(self):
        ret = True

        for leaf in self.children:
            if not isinstance(leaf,Node):
                continue
            if len(leaf.children) != 0:
                ret = False
        return ret

# Gan methods start


def add_second_quote(part):
    # Gan 16.11.17
    one_bracket_pattern = re.compile('".*[^"]$')
    for i in range(0, len(part)):
        if one_bracket_pattern.match(part[i]) is not None:
            part[i] = part[i] + '"'

# Gan methods end

def command_splitter(list):
    reslist = []
    sublist = []
    args = 0
    for i in range(0,len(list)):
        if list[i].lower() in dict:
            list[i] = list[i].lower()

        if list[i] in dict:
            args += dict[list[i]]['args']
            if args == 0:
                sublist.append(list[i])
                if i < len(list)-1 and list[i+1] in infs:
                    args += 1
                    continue
                reslist.append(sublist)
                sublist = []
            else:
                sublist.append(list[i])
                if i < len(list)-1 and list[i+1] in infs:
                    args += 1
                    continue
                args -= 1
        else:
            if args == 0:
                sublist.append(list[i])
                if i < len(list)-1 and list[i+1] in infs:
                    args += 1
                    continue

                reslist.append(sublist)
                sublist = []
            else:
                sublist.append(list[i])
                if i < len(list)-1 and list[i+1] in infs:
                    args += 1
                    continue
                args -= 1

    if sublist != []:
        reslist.append(sublist)
    print('command_splitter:') 
    print(''.join(str(c)+'\n' for c in reslist))
    return reslist

# проблема с двойными скобками типа "2))" // Частично решена
def bracket_splitter(list):
    reslist = []
    for word in list:
        if word[0] == '[' and word[-1] == ']':
            reslist.append(word)
            continue
        if word[0] == '[':
            subelem = word
            count_l = 1
            count_r = 0
            mark = 0
            for tmp in range(list.index(word)+1,len(list)):
                if list[tmp][0] == '[': count_l += 1
                if list[tmp][-1] == ']':
                    count_r += 1
                    list[tmp] = list[tmp].replace(']',' ]')
                subelem += ' ' + list[tmp]
                mark = tmp
                if count_l == count_r: break

            reslist.append(subelem)
            del list[0:mark+1]
            reslist += bracket_splitter(list)
            break
        else:
            reslist.append(word)

    return reslist

def bracket_splitter_round(list):
    reslist = []
    for word in list:
        if word[0] == '(' and word[-1] == ')': continue
        if word[0] == '(':
            subelem = word
            count_l = 1
            count_r = 0
            mark = 0
            for tmp in range(list.index(word)+1,len(list)):
                if list[tmp][0] == '(': count_l += 1
                elif list[tmp][-1] == ')': count_r += 1
                subelem += ' ' + list[tmp]
                mark = tmp
                if count_l == count_r: break

            reslist.append(subelem)
            del list[0:mark+1]
            reslist += bracket_splitter(list)
            break
        else:
            reslist.append(word)

    return reslist

# построение дерева в глубину
def constructing_deep(source_list, endless = False):
    print('to constructing_deep:') 
    print(source_list)
    if len(source_list) == 0: return None

    if len(source_list) == 1:
        tmp = Node(source_list[0])
        del source_list[0]
        return builder(tmp) # ?????

    if source_list[0] in dict and dict[source_list[0]]['args'] != 0:
        tree = Node(source_list[0])
        key = source_list[0]
        del source_list[0:1]
        if endless is False:
            if(len(source_list) < dict[tree.value]['args']):
                print("The number of arguments for function \"", tree.value, "\" is incorrect: Expected", dict[tree.value]['args'], ",got", len(source_list))
                exit(1)

            for i in range(0,dict[tree.value]['args']):
                if key not in ['everyone', 'forever', 'if', 'repeat', 'ifelse', 'pick'] and re.match('\[', source_list[0]) != None:
                    source_list[0] = source_list[0][1:]
                    countl = 1
                    countr = 0
                    temp = list(source_list[0])
                    for i in range(0,len(temp)):
                        if temp[i] == ']':
                            countr += 1
                        elif temp[i] == '[':
                            countl += 1
                        if countl == countr:
                            temp[i] = ''
                            break

                    temp = ''.join(temp)
                    source_list[0] = temp
                    list1 = split(source_list[0])
                    for j in range(0,len(list1)):
                        list1[j] = list1[j].replace('"','\\\"')
                        list1[j] = list1[j].replace('|', '')
                        list1[j] = '"' + list1[j] + '"'
                    result = ','.join(list1)
                    result = '[' + result + ']'
                    tree.children.append(Node(result))
                    del source_list[0]
                    continue
                if re.match('\[', source_list[0]) != None:
                    source_list[0] = source_list[0][1:]
                    countl = 1
                    countr = 0
                    temp = list(source_list[0])
                    for i in range(0,len(temp)):
                        if temp[i] == ']':
                            countr += 1
                        elif temp[i] == '[':
                            countl += 1
                        if countl == countr:
                            temp[i] = ''
                            break

                    temp = ''.join(temp)
                    source_list[0] = temp
                    list1 = source_list[0].split()
                    list1_new = []
                    for i in range(0, len(list1)):
                        if list1[i][-1] == ',':
                            if list1[i][0] == '[':
                                list1_new.append("[tto")
                                list1_new.append('"' + list1[i][1:].replace(',', ''))
                            else:
                                list1_new.append("tto")
                                list1_new.append('"' + list1[i].replace(',', ''))
                        else:
                            list1_new.append(list1[i])

                    list1 = list1_new

                    # Gan 16.11.17
                    add_second_quote(list1)

                    del source_list[0]
                    array = []
                    list1 = bracket_splitter(bracket_splitter_round(list1))

                    #TODO: Wrong trees when nested lists.

                    for lst in command_splitter(list1):
                        array.append(constructing_deep(lst))
                    tree.children.append(array)
                else:
                    tree.children.append(constructing_deep(source_list))
        else:
            while source_list != []:
                tree.children.append(constructing_deep(source_list))

        return tree
    else:
        if source_list[1] not in infs:
            tree = Node(source_list[0])
            del source_list[0:1]
            return tree

        const = source_list[0]
        inf = source_list[1]
        del source_list[0:2]
        tmp_tree = constructing_deep(source_list)
        if tmp_tree == None:
            tree = Node(inf)
            tree.children.append(Node(const))
            return tree
        elif len(tmp_tree.children) == 0:
            tree = Node(inf)
            tree.children.append(Node(const))
            tree.children.append(tmp_tree)
            return tree
        elif tmp_tree.value in dict:
            tree = Node(inf)
            tree.children.append(Node(const))
            tree.children.append(tmp_tree)
            return tree
        elif tmp_tree.value in infs:
            if infs[tmp_tree.value]['priority'] >= infs[inf]['priority']:
                tree = Node(inf)
                tree.children.append(Node(const))
                tree.children.append(tmp_tree)
                return tree
            else:
                tmp = tmp_tree.children[0].value
                tmp_tree.children[0] = constructing_deep([const,inf,tmp])
                return tmp_tree

def reconstr(tree):
    if tree.value in ['everyone', 'forever','if','repeat','ifelse','pick']: return tree
    for child in tree.children:
        if child.value[0] == '(' and child.value[-1] == ')':
            endless = False
            if re.match("\(sum",child.value) != None or re.match("\(sentence",child.value) != None:
                endless = True
            l = list(child.value)
            l[0] = ''
            l[-1] = ''

            child.value = "".join(l)
            tree.children.remove(child)
            chx = bracket_splitter(bracket_splitter_round(child.value.split()))
            child = constructing_deep(chx,endless=endless)
            tree.add_child(child)
        else:
            child = reconstr(child)

    return tree


# Собирает из дерева выражение на JS
def builder(tree):
    str = ''
    if len(tree.children) == 0:
        if tree.value in dict:
            if dict[tree.value]['trans'] == "userproc":
                tree.value = "yield* prims.userproc(\"" + tree.value + "\", [])"
            else:
                tree.value = dict[tree.value]['trans']
        return tree

    if tree.isLeafHead():
        strs = []
        if tree.value in infs:
            strs.append('(')
            if tree.children[0].value in dict:
                strs.append(dict[tree.children[0].value]['trans'])
            else:
                strs.append(tree.children[0].value)

            strs.append(infs[tree.value]['trans'])

            if tree.children[1].value in dict:
                strs.append(dict[tree.children[1].value]['trans'])
            else:
                strs.append(tree.children[1].value)
            strs.append(')')
        else:
            if tree.value == 'forever':
                strs.append(dict['forever']['trans'])
                for item in tree.children[0]:

                    for ch in item.children:
                        if isinstance(ch, list):
                            for part in range(0, len(ch)):
                                ch[part] = builder(ch[part])

                        elif len(ch.children) != 0:
                            ch = builder(ch)

                    strs.append(builder(item).value)
                    strs.append(';\n')
                strs.append('});')

            elif tree.value == 'everyone':
                strs.append(dict['everyone']['trans'])
                for item in tree.children[0]:
                    for ch in item.children:
                        if isinstance(ch, list):
                            for part in range(0, len(ch)):
                                ch[part] = builder(ch[part])

                        elif len(ch.children) != 0:
                            ch = builder(ch)

                    strs.append(builder(item).value)
                    strs.append(';\n')
                strs.append('});')

            elif tree.value == 'if':
                strs.append('if (')
                if tree.children[0].value in dict:
                    strs.append(dict[tree.children[0].value]['trans'])
                else:
                    strs.append(tree.children[0].value)

                strs.append(') {\n\t')
                #if not isinstance(tree.children[1], Node):
                for item in tree.children[1]:

                    for ch in item.children:
                        if isinstance(ch, list):
                            for part in range(0, len(ch)):
                                ch[part] = builder(ch[part])

                        elif len(ch.children) != 0:
                            ch = builder(ch)

                    strs.append(builder(item).value)
                    strs.append(';\n')

                strs.append('\n}')
            elif tree.value == 'ifelse':
                strs.append('if (')
                if tree.children[0].value in dict:
                    strs.append(dict[tree.children[0].value]['trans'])
                else:
                    strs.append(tree.children[0].value)

                strs.append(') {\n\t')

                for item in tree.children[1]:

                    for ch in item.children:
                        if isinstance(ch, list):
                            for part in range(0, len(ch)):
                                ch[part] = builder(ch[part])

                        elif len(ch.children) != 0:
                            ch = builder(ch)

                    strs.append(builder(item).value)
                    strs.append(';\n')

                strs.append('\n} else {\n')

                for item in tree.children[2]:

                    for ch in item.children:
                        if isinstance(ch, list):
                            for part in range(0, len(ch)):
                                ch[part] = builder(ch[part])

                        elif len(ch.children) != 0:
                            ch = builder(ch)

                    strs.append(builder(item).value)
                    strs.append(';\n')

                strs.append('\n}')
            elif tree.value == 'repeat':
                strs.append(dict['repeat']['trans'])
                if tree.children[0].value in dict:
                    strs.append(dict[tree.children[0].value]['trans'])
                else:
                    strs.append(tree.children[0].value)

                strs.append('; i>0; i--){\n\t')
                for item in tree.children[1]:

                    for ch in item.children:
                        if isinstance(ch, list):
                            for part in range(0,len(ch)):
                                ch[part] = builder(ch[part])

                        elif len(ch.children) != 0:
                            ch = builder(ch)

                    strs.append(builder(item).value)
                    strs.append(';\n')

                strs.append('\n}')

            elif tree.value == 'tto':
                strs.append(dict[tree.value]['trans'])
                for ch in tree.children:
                    strs.append(ch.value)
                    strs.append(',')
                del strs[-1]
                strs.append(')')

            elif tree.value == 'pick':
                strs.append(dict[tree.value]['trans'] + "([")
                for item in tree.children[0]:

                    for ch in item.children:
                        if isinstance(ch, list):
                            for part in range(0, len(ch)):
                                ch[part] = builder(ch[part])

                        elif len(ch.children) != 0:
                            ch = builder(ch)

                    strs.append("\"" + builder(item).value + "\"")
                    strs.append(",")
                del strs[-1]
                strs.append('])')

            else:
                if dict[tree.value]['trans'] == "userproc":
                    strs.append("yield* prims.userproc("+"\""+tree.value+"\""+", ")
                    strs.append('[')
                    for ch in tree.children:
                        if ch.value in dict:
                            strs.append(dict[ch.value]['trans'])
                        else:
                            strs.append(ch.value)
                        strs.append(',')
                    del strs[-1]
                    strs.append('])')
                else:
                    strs.append(dict[tree.value]['trans'])
                    strs.append('(')
                    for ch in tree.children:
                        if ch.value in dict:
                            strs.append(dict[ch.value]['trans'])
                        else:
                            strs.append(ch.value)
                        strs.append(',')
                    del strs[-1]
                    strs.append(')')
                    if ''.join(strs).count("(") != ''.join(strs).count(")"):      # set
                        strs.append(')')

        tree.children = []
        tree.value = ''.join(strs)
        return tree

    for ch in tree.children:
        if not isinstance(ch,Node):
                continue
        if len(ch.children) == 0:
            continue

        ch = builder(ch)

    return tree

def slash_splitter(lst):  # |A u B|
    reslist = []
    tmpstr = ''
    flag = False
    for i in range (0,len(lst)):
        if re.match("\|",lst[i]) != None:
            if len(lst[i]) == 1 and flag:
                flag = False
                reslist.append(tmpstr)
                continue
            tmpstr += lst[i][1:]
            flag = True
        elif re.match("\"\|",lst[i]) != None:
            tmpstr += lst[i][2:]
            flag = True
        elif lst[i][-1] == '|':
            tmpstr += lst[i][:-1]
            flag = False
            reslist.append(tmpstr)
        elif flag == False:
            reslist.append(lst[i])
        elif flag:
            tmpstr += ' ' + lst[i] + ' '
    return reslist

def split(str1):
    print(str1)
    reslist = []
    tmpstr = ''
    flag = False

    for i in range(0,len(str1)):
        if str1[i] == "|" and flag == False:
            flag = True
            tmpstr += str1[i]
        elif flag and str1[i] != "|":
            tmpstr += str1[i]
        elif flag == False and str1[i] == ' ':
            if tmpstr == '':
                continue
            reslist.append(tmpstr)
            tmpstr = ''
        elif flag and str1[i] == "|":
            flag = False
            tmpstr += str1[i]
            reslist.append(tmpstr)
            tmpstr = ''
        elif flag == False and str1[i] != ' ':
            tmpstr += str1[i]
        if i == len(str1) - 1 and tmpstr != '':
            reslist.append(tmpstr)
    print('hi there\n', str(reslist), '\n')
    return reslist

def variable_arguments_reconstruction(str):
    result = ""

    return result

######################### 1

def start(str):
    list_full = split(str)

    tree_list = []
    print('hi again1\n', list_full,'\n')
    list_full = bracket_splitter_round(bracket_splitter(list_full))
    print('hi again2\n', list_full,'\n')
    list_full_new = []
    for i in range(0,len(list_full)):
        if list_full[i][-1] == ',':
            list_full_new.append("tto")
            list_full_new.append('"' + list_full[i].replace(',',''))
        else:
            list_full_new.append(list_full[i])
    print('hi again3\n', list_full_new,'\n')

    list_full = list_full_new
    for lst in command_splitter(list_full):
        # lst = slash_splitter(lst)
        for i in range (0,len(lst)):
            if re.match('"',lst[i]) != None:
                lst[i] = lst[i] + '"'

        for i in range (0,len(lst)):
            if re.match(':',lst[i]) != None:
                lst[i] = lst[i][1:]
                lst[i] = 'scope.thing(' + '"' + lst[i] + '"' + ')'
            if re.match('\[',lst[i]) != None:
                tmp = lst[i][1:-1]
                tmp = tmp.split()
                for j in range (0,len(tmp)):
                    if re.match(':',tmp[j]) != None:
                        tmp[j] = tmp[j][1:]
                        tmp[j] = 'scope.thing(' + '"' + tmp[j] + '"' + ')'
                lst[i] = '[' + ' '.join(tmp) + ']'
        tree = constructing_deep(lst)

        if lst != []:
            print("Don't know want to do with:",' '.join(lst))
            exit(2)
        tree = reconstr(reconstr(reconstr(reconstr(tree))))
        try:
            if dict[tree.value]['args'] == 0:
                tree = builder(tree)
            else:
                while tree.children != []:
                    tree = builder(tree)
        except:
            while tree.children != []:
                tree = builder(tree)

        tree_list.append(tree)

    return tree_list

print("MWX Parser&Compiler v0.0.61")