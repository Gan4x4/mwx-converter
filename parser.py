#! /usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'duckoteka'

import re
import sys
import struct
import json
import base64
import main
import rtf
from dictioanry import dict, infs
import zlib
import os

# for key, value in infs.items():
#     print(key)

testingPrint = 1

class Object:
    def to_JSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, indent=4, ensure_ascii=False)

head = "function* (globals, processContext, obj){\nvar prims = processContext.prims;\nvar vars = globals.vars;\nvar userproc = globals.userproc;\nvar scope = vars.globalScope;\n\n"

fo = open('out/data.js', 'w')

fhead = open('library.lld', 'rb')  # Оглавление
fbase = open('library.dll', 'rb')  # Библиотека

fo.write("projectData = ")

dll_head = {}
while 1:
    off = fhead.read(34).decode('utf-16')
    if len(off) == 0:
        break
    key = fhead.read(66).decode('utf-16')
    off = struct.unpack("i", fhead.read(4))[0]
    dll_head[key] = off
                    # Оглавление в виде словоря: Хэш_32 - оффсет

argFileName = sys.argv[1]

f = open("js-test.dat", 'wb')
file_len = 0
with open(argFileName, mode='rb') as file:
    fileContent = file.read()
    start = fileContent[:138].decode("utf-16").find('[')
    end = fileContent[:138].decode("utf-16").find(']')

    projectsize = fileContent[start*2+2:end*2].decode("utf-16")

    if argFileName[len(argFileName)-3:] == 'mj3':
        data = zlib.decompress(fileContent[184+len(projectsize)*2:])
    else:
        data = zlib.decompress(fileContent[124+len(projectsize)*2:])
    f.write(data)
    file_len = len(data)
    f.close()

f = open("js-test.dat", 'rb')

#first run

cur = 32
while 1:
    f.seek(cur)
    off = f.read(4)
    off = struct.unpack("i", off)[0]

    content = f.read(off - 4)
    try:
        content = content.decode("utf-16")
    except:

        f.seek(cur + 4)
        xxx = f.read(300)
        xxx = xxx.decode("utf-16")

        content = xxx
    cur += off
    if re.match('procedures', content) is not None:
        loc_cur = cur - off + 4
        f.seek(loc_cur)
        procTmp = f.read(22).decode("utf-16")
        f.seek(loc_cur + 22)
        loc_off = 0
        loc_off = f.read(4)
        loc_off = struct.unpack("i", loc_off)[0]

        loc_con = f.read(loc_off - 4)

        s = loc_con[10:]
        procs = rtf.striprtf(s)

        procsarray = []
        procsarray.append([])
        i = 0
        for st in procs.split('\n'):
            if st == '':
                continue
            if st != 'end':

                procsarray[i].append(st) # ?
            else:
                procsarray.append([])
                i += 1


        del procsarray[-1]

        if procsarray == []:
            continue


        for proc in procsarray:
            proc_head = main.split(proc[0])
            del proc_head[0]
            proc_name = proc_head[0]
            del proc_head[0]
            proc_class = 1
            proc_args = len(proc_head)

            proc_trans = "userproc"
            dict[proc_name] = {
                'class' : proc_class,
                'args' : proc_args,
                'trans' : proc_trans,
            }

    elif re.match('page', content) is not None or re.match('templatepage', content) is not None:
        loc_obj = Object()
        loc_obj.Object = []
        loc_cur = cur - off + 4
        f.seek(loc_cur)
        if re.match('page', content) is not None:
            x = f.read(10)
            loc_cur += 10
        else:
            x = f.read(26)
            loc_cur += 26

        while 1:  # fix bounds -
            loc_off = f.read(4)
            try:
                loc_off = struct.unpack("i", loc_off)[0]
            except:
                break
            loc_cur += loc_off
            loc_con = f.read(loc_off - 4)
            try:
                loc_con_dec = loc_con.decode('utf-16')
            except:
                loc_con_dec = loc_con[0:14].decode('utf-16')


            if loc_con_dec == "ObjectP":
                loc_con_dec = "ObjectPro"

            if re.match('object', loc_con_dec) is not None: # b
                new_obj = Object()
                loc_con = loc_con[14:]
                while 1:
                    loc_off = loc_con[:4]
                    if len(loc_off) != 4:
                        break
                    loc_off = struct.unpack("i", loc_off)[0]
                    res = loc_con[4:loc_off]
                    loc_con = loc_con[loc_off:]

                    try:  # Windows rtf adds \x00 at the end
                        tmp_dec = res.decode('utf-16')
                    except:
                        res = res[:-1]

                    if re.match('name', res[:10].decode('utf-16')) is not None:
                        new_obj.name = res[10:].decode('utf-16')

                    if re.match('type', res[:10].decode('utf-16')) is not None:
                        new_obj.type = res[10:].decode('utf-16')

                    if re.match('text', res[:10].decode('utf-16')) is not None:
                        new_obj.text = rtf.striprtf(res[10:])

                    if re.match('data', res[:10].decode('utf-16')) is not None:
                        try:
                            fm = open(new_obj.name + ".wav","xb")
                        except:
                            fm = open(new_obj.name + ".wav", "wb")
                        fm.write(res[10:])
                        fm.close()

                    if re.match('locked', res[:14].decode('utf-16')) is not None:
                        new_obj.showname = res[14:].decode('utf-16')


                if new_obj.type == "text":
                    dict[new_obj.name.lower()] = {
                        'class': 0,
                        'args': 0,
                        'trans': "prims.get(\"" + new_obj.name.lower() + "\", \"text\")",
                    }

                    dict["set" + new_obj.name.lower()] = {
                        'class': 1,
                        'args': 1,
                        'trans': "yield* prims.set(\"" + new_obj.name.lower() + "\", \"text\",",
                    }
                elif new_obj.type == "record":
                    dict[new_obj.name.lower()] = {
                        'class': 1,
                        'args': 0,
                        'trans': "yield* prims.audiostart(\"" + new_obj.name.lower() + "\")",
                    }



            if re.match('ObjectPro', loc_con_dec) is not None: # t
                new_obj = Object()
                loc_con = loc_con[20:]
                while 1:
                    loc_off = loc_con[:4]
                    if len(loc_off) != 4:
                        break
                    loc_off = struct.unpack("i", loc_off)[0]
                    res = loc_con[4:loc_off]
                    resforproc = loc_con[4:loc_off]
                    loc_con = loc_con[loc_off:]
                    try:
                        x = res.decode('utf-16')
                    except:
                        res = res[:22]


                    if re.match('ShapesProcs', res.decode('utf-16')) is not None:
                        new_obj.shapesprocs = []

                        procs = rtf.striprtf(resforproc[22+2+4+8+2:])

                        procsarray = []
                        procsarray.append([])
                        i = 0
                        for st in procs.split('\n'):
                            if st == '':
                                continue
                            if st != 'end':
                                procsarray[i].append(st) # ?
                            else:
                                procsarray.append([])
                                i += 1
                        if len(procsarray) != 1:
                            del procsarray[-1]


                        for proc in procsarray:
                            proc_head = main.split(proc[0])

                            del proc_head[0]
                            proc_name = proc_head[0]
                            del proc_head[0]
                            proc_class = 1
                            proc_args = len(proc_head)

                            proc_trans = "userproc"
                            dict[proc_name] = {
                                'class' : proc_class,
                                'args' : proc_args,
                                'trans' : proc_trans,
                            }

                    if loc_cur >= cur:
                        break


    if cur >= file_len:
        break

#####
##### pragma mark - Main loop

f.close()
f = open('js-test.dat', 'rb')

cur = 32
obj = Object()
obj.pages = []
while 1:
    f.seek(cur)
    off = f.read(4)
    off = struct.unpack("i", off)[0]

    content = f.read(off - 4)
    try:
        content = content.decode("utf-16")
    except:

        f.seek(cur + 4)
        xxx = f.read(300)
        xxx = xxx.decode("utf-16")

        content = xxx

    cur += off
    if re.match('projectsize', content) is not None:  # Done
        content = content[len('projectsize['):]
        content = content.replace('[', '')
        content = content.replace(']', '')
        content = content.split()
        for i in range(0, len(content)):
            content[i] = int(content[i])
        obj.projectsize = content
    elif re.match('procedures', content) is not None:
        if testingPrint: print("\nProcedures:")

        obj.procedures = []

        loc_cur = cur - off + 4
        f.seek(loc_cur)
        procTmp = f.read(22).decode("utf-16")
        f.seek(loc_cur + 22)
        loc_off = 0
        loc_off = f.read(4)
        loc_off = struct.unpack("i", loc_off)[0]

        loc_con = f.read(loc_off - 4)

        s = loc_con[10:]
        procs = rtf.striprtf(s)

        procsarray = []
        procsarray.append([])
        i = 0
        for st in procs.split('\n'):
            if st == '':
                continue
            if st != 'end':
                procsarray[i].append(st) # ?
            else:
                procsarray.append([])
                i += 1


        del procsarray[-1]

        if procsarray == []:
            if testingPrint: print("\tNo prosedures\n")
            continue

        for proc in procsarray:
            proc_head = main.split(proc[0])
            del proc_head[0]
            proc_name = proc_head[0]
            del proc_head[0]
            proc_class = 1
            proc_args = len(proc_head)

            proc_trans = "userproc"
            dict[proc_name] = {
                'class' : proc_class,
                'args' : proc_args,
                'trans' : proc_trans,
            }
            args_list = proc_head

            proc_obj = Object()
            del proc[0]
            proc_str = ' '.join(proc)

            if proc_str != '':
                code = main.start(proc_str)
            else:
                code = ''

            if testingPrint: print("\t", procsarray.index(proc) + 1, "\tName:", proc_name,
                                   "\n\t\tArgs:", ' '.join(proc_head), "\n\t\tCode:", proc_str,
                                   "\n\t\tJS Code:\n", ' '.join(str(c) for c in code))#.replace('\n','') 

            args_str = ""
            for i in range(0,proc_args):
                args_list[i] = args_list[i].replace(':','')
            for arg in args_list:
                args_str += "\"" + arg + "\""
                if args_list[-1] != arg:
                    args_str += ","

            code_str = "function* (args){\nvar scope = vars.newLocalScope();\nscope.setArgs(["+args_str+ "],args);\n"
            if code != '':
                for i in code:
                    code_str += i.value + "\n"
            code_str += "}"

            proc_obj.code = code_str
            proc_obj.name = proc_name
            obj.procedures.append(proc_obj)


    elif re.match('Bitmaps', content) is not None:  # Done
        if testingPrint: print("\nBitmaps:")
        loc_cur = cur - off + 4
        f.seek(loc_cur)
        tmpBitmap = f.read(16).decode("utf-16")
        loc_cur += 16
        loc_off = 0
        obj.Bitmaps = []
        while loc_cur + 4 < cur:  # fix boarders
            loc_off = f.read(4)
            loc_off = struct.unpack("i", loc_off)[0]

            loc_con = f.read(loc_off - 4)
            loc_obj = Object()
            loc_obj.hash = loc_con[0:66].decode('utf-16')  # hash
            loc_obj.data = str(base64.b64encode(loc_con[80:loc_off - 4]))[2:-1]  # data

            obj.Bitmaps.append(loc_obj)
            loc_cur += loc_off + 6

        if testingPrint:
            if obj.Bitmaps == []: print('\tEmpty')
            else: print('\tLoaded')

    elif re.match('GlobalShapes', content) is not None:
        loc_cur = cur - off + 4
        f.seek(loc_cur)
        tmpGS = f.read(26).decode("utf-16")
        f.seek(loc_cur + 26)
        loc_off = f.read(4)
        loc_off = struct.unpack("i", loc_off)[0]
        loc_con = f.read(loc_off - 4).decode('utf-16')
        tmp_hash = loc_con.split(' ')
        del tmp_hash[0:1]

        loc_off = f.read(4)
        loc_off = struct.unpack("i", loc_off)[0]
        loc_con = f.read(loc_off - 4).decode('utf-16')
        tmp_names = loc_con.split(' ')
        del tmp_names[0:1]

        obj.GlobalShapes = []

        j = 0
        while j != len(tmp_names):
            if j % 2 == 0:
                if tmp_names[j].isdigit() is False:
                    tmp_names[j - 1] = " ".join([tmp_names[j - 1], tmp_names[j]])
                    del tmp_names[j]
                    continue
            j += 1

        for i in range(0, len(tmp_names), 2):
            loc_obj = Object()

            loc_obj.number = int(tmp_names[i])

            tmp_names[i + 1] = tmp_names[i + 1].replace('|', '')
            tmp_names[i + 1] = tmp_names[i + 1].replace('[', '')
            tmp_names[i + 1] = tmp_names[i + 1].replace(']', '')
            loc_obj.name = tmp_names[i + 1]
            if i + 1 < len(tmp_hash):
                loc_obj.hash = tmp_hash[i + 1].replace(']','').replace('[','')
            else:
                loc_obj.hash = ''

            obj.GlobalShapes.append(loc_obj)
            if loc_obj.hash != '':
                if loc_obj.hash in dll_head:
                    bmp_obj = Object()
                    bmp_obj.hash = loc_obj.hash
                    fbase.seek(dll_head[bmp_obj.hash])
                    data_off = fbase.read(4)
                    data_off = struct.unpack("i", data_off)[0]
                    bmp_obj.data = base64.b64encode(fbase.read(data_off - 4)).hex()
                    obj.Bitmaps.append(bmp_obj)


    elif re.match('page', content) is not None or re.match('templatepage', content) is not None:
        loc_obj = Object()
        loc_obj.Object = []
        loc_cur = cur - off + 4
        f.seek(loc_cur)
        if re.match('page', content) is not None:
            x = f.read(10)
            loc_cur += 10
        else:
            x = f.read(26)
            loc_cur += 26

        while 1:  # fix bounds
            loc_off = f.read(4)
            loc_off = struct.unpack("i", loc_off)[0]
            loc_cur += loc_off
            loc_con = f.read(loc_off - 4)
            try:
                loc_con_dec = loc_con.decode('utf-16')
            except:
                loc_con_dec = loc_con[0:14].decode('utf-16')

            if loc_con_dec == "ObjectP":
                loc_con_dec = "ObjectPro"

            if loc_con_dec == "freezeb":
                loc_con_dec = "freezebg"


            if re.match('name', loc_con_dec) is not None:
                loc_obj.name = loc_con[10:].decode('utf-16')

            if re.match('curturtle', loc_con_dec) is not None:
                loc_obj.curturtle = loc_con[20:].decode('utf-16')

            if re.match('curtext', loc_con_dec) is not None:
                loc_obj.curtext = loc_con[16:].decode('utf-16')

            if re.match('transition', loc_con_dec) is not None:
                loc_obj.transition = loc_con[22:].decode('utf-16')

            if re.match('bg', loc_con_dec) is not None:
                if re.match('bg_alpha', loc_con_dec) is None:
                    loc_obj.bg = loc_con[6:].decode('utf-16')

            if re.match('bg_alpha', loc_con_dec) is not None:  # fix bg confuse
                loc_obj.bg_alpha = loc_con[18:].decode('utf-16')

            if re.match('buffer', loc_con_dec) is not None:
                loc_obj.buffer = str(base64.b64encode(loc_con[14:]))[2:-1]

            if re.match('freezebg', loc_con_dec) is not None:
                loc_obj.freezebg = str(base64.b64encode(loc_con[18:]))[2:-1]

            if re.match('object', loc_con_dec) is not None: # b
                new_obj = Object()
                loc_con = loc_con[14:]
                while 1:
                    loc_off = loc_con[:4]
                    if len(loc_off) != 4:
                        break
                    loc_off = struct.unpack("i", loc_off)[0]
                    res = loc_con[4:loc_off]
                    loc_con = loc_con[loc_off:]

                    try:  # Windows rtf adds \x00 at the end
                        tmp_dec = res.decode('utf-16')
                    except:
                        res = res[:-1]

                    if re.match('name', res[:10].decode('utf-16')) is not None:
                        new_obj.name = res[10:].decode('utf-16')
                        new_obj.filename = new_obj.name + ".wav"

                    if re.match('type', res[:10].decode('utf-16')) is not None:
                        new_obj.type = res[10:].decode('utf-16')

                    if re.match('rect', res[:10].decode('utf-16')) is not None:
                        tmp = res[12:-2].decode('utf-16').split()
                        new_obj.rect = Object()
                        new_obj.rect.xpos = tmp[0]
                        new_obj.rect.ypos = tmp[1]
                        new_obj.rect.width = tmp[2]
                        new_obj.rect.height = tmp[3]

                    if re.match('label', res[:12].decode('utf-16')) is not None:
                        new_obj.label = res[12:].decode('utf-16')

                    if re.match('show-name', res[:22].decode('utf-16')) is not None:
                        new_obj.show_name = res[22:].decode('utf-16')

                    if re.match('locked', res[:16].decode('utf-16')) is not None:
                        new_obj.locked = res[16:].decode('utf-16')

                    if re.match('visible', res[:18].decode('utf-16')) is not None:
                        new_obj.visible = res[18:].decode('utf-16')

                    if re.match('snaped', res[:16].decode('utf-16')) is not None:
                        new_obj.snaped = res[16:].decode('utf-16')

                    if re.match('singleline', res[:24].decode('utf-16')) is not None:
                        new_obj.singleline = res[24:].decode('utf-16')

                    if re.match('text', res[:10].decode('utf-16')) is not None:
                        new_obj.text = rtf.striprtf(res[10:])

                    if re.match('fcn', res[:8].decode('utf-16')) is not None:
                        new_obj.fcn = Object()
                        name = res[10:-2].decode('utf-16')
                        code_str = ''
                        if name != '':
                            code = main.start(name)

                            for i in code:
                                code_str += i.value + "\n"
                        if name != '':
                            new_obj.fcn.code = head + code_str + "};"

                    if re.match('kind', res[:10].decode('utf-16')) is not None:
                        name = res[10:].decode('utf-16')
                        name = int(name)
                        if name == 1:
                            new_obj.fcn.mode = "once"
                        elif name == 0:
                            new_obj.fcn.mode = "forever"


                loc_obj.Object.append(new_obj)

            if re.match('ObjectPro', loc_con_dec) is not None: # t
                new_obj = Object()
                loc_con = loc_con[20:]
                while 1:
                    loc_off = loc_con[:4]
                    if len(loc_off) != 4:
                        break
                    loc_off = struct.unpack("i", loc_off)[0]
                    res = loc_con[4:loc_off]
                    resforproc = loc_con[4:loc_off]
                    loc_con = loc_con[loc_off:]
                    try:
                        x = res.decode('utf-16')
                    except:
                        res = res[:22]


                    if re.match('ShapesProcs', res.decode('utf-16')) is not None:
                        new_obj.shapesprocs = []

                        procs = rtf.striprtf(resforproc[22+2+4+8+2:])

                        procsarray = []
                        procsarray.append([])
                        i = 0
                        for st in procs.split('\n'):
                            if st == '':
                                continue
                            if st != 'end':
                                procsarray[i].append(st)
                            else:
                                procsarray.append([])
                                i += 1
                        if len(procsarray) != 1:
                            del procsarray[-1]


                        for proc in procsarray:
                            proc_head = main.split(proc[0])
                            del proc_head[0]
                            proc_name = proc_head[0]
                            del proc_head[0]
                            proc_class = 1
                            proc_args = len(proc_head)

                            proc_trans = "userproc"
                            dict[proc_name] = {
                                'class' : proc_class,
                                'args' : proc_args,
                                'trans' : proc_trans,
                            }
                            args_list = proc_head

                            proc_obj = Object()
                            del proc[0]
                            proc_str = ' '.join(proc)

                            code = main.start(proc_str)
                            args_str = ""
                            for i in range(0,proc_args):
                                args_list[i] = args_list[i].replace(':','')
                            for arg in args_list:
                                args_str += "\"" + arg + "\""
                                if args_list[-1] != arg:
                                    args_str += ","

                            code_str = "function* (args){\nvar scope = " \
                                                                       "vars.newLocalScope();\nscope.setArgs(["+args_str+ "],args);\n"
                            for i in code:
                                code_str += i.value + "\n"
                            code_str += "}"

                            proc_obj.code = code_str
                            proc_obj.name = proc_name
                            new_obj.shapesprocs.append(proc_obj)

                    if re.match('mapshapenumber', res.decode('utf-16')) is not None:
                        new_obj.mapshapenumber = res[30:].decode('utf-16')[1:-1]
                        str123 = new_obj.mapshapenumber.split()
                        del str123[0]
                        new_obj.mapshapenumber = []
                        for i in range(0, len(str123), 2):
                            tmp_obj = Object()
                            tmp_obj.number = int(str123[i])
                            tmp_obj.hash = str123[i + 1]
                            new_obj.mapshapenumber.append(tmp_obj)

                    if re.match('name', res.decode('utf-16')) is not None:
                        new_obj.name = res[10:].decode('utf-16')

                    if re.match('size', res.decode('utf-16')) is not None:
                        new_obj.size = res[10:].decode('utf-16')

                    if re.match('pencolor', res.decode('utf-16')) is not None:
                        new_obj.pencolor = res[18:].decode('utf-16')

                    if re.match('pensize', res.decode('utf-16')) is not None:
                        new_obj.pensize = res[16:].decode('utf-16')

                    if re.match('penstate', res.decode('utf-16')) is not None:
                        new_obj.penstate = res[18:].decode('utf-16')

                    if re.match('heading', res.decode('utf-16')) is not None:
                        new_obj.heading = res[16:].decode('utf-16')

                    if re.match('type', res.decode('utf-16')) is not None:
                        new_obj.type = res[10:].decode('utf-16')

                    if re.match('xpos', res.decode('utf-16')) is not None:
                        new_obj.xpos = res[10:].decode('utf-16')

                    if re.match('ypos', res.decode('utf-16')) is not None:
                        new_obj.ypos = res[10:].decode('utf-16')

                    if re.match('shape', res.decode('utf-16')) is not None:
                        if re.match('shapenames', res.decode('utf-16')) is None and re.match('shapein', res.decode('utf-16')) is None:
                            new_obj.shape = res[12:].decode('utf-16')

                    if re.match('shapenames', res.decode('utf-16')) is not None:
                        new_obj.shapenames = res[22:].decode('utf-16')[1:-1]
                        str123 = new_obj.shapenames.split()
                        del str123[0]
                        new_obj.shapenames = []
                        for i in range(0,len(str123),2):
                            tmp_obj = Object()
                            tmp_obj.number = int(str123[i])
                            tmp_obj.name = str123[i+1][1:-1]
                            new_obj.shapenames.append(tmp_obj)

                    if re.match('shapein', res.decode('utf-16')) is not None:
                        new_obj.shapein = res[16:].decode('utf-16')

                    if re.match('colorin', res.decode('utf-16')) is not None:
                        new_obj.colorin = res[16:].decode('utf-16')

                    if re.match('opacity', res.decode('utf-16')) is not None:
                        new_obj.opacity = res[16:].decode('utf-16')

                    if re.match('shown', res.decode('utf-16')) is not None:
                        new_obj.shown = res[14:].decode('utf-16')

                    if re.match('locked', res.decode('utf-16')) is not None:
                        new_obj.locked = res[16:].decode('utf-16')

                    if re.match('ontimer', res.decode('utf-16')) is not None:
                        name = res[18:-2].decode('utf-16')

                        name_list = name.split()
                        cur_time = name_list[0]
                        tic = name_list[1]
                        del name_list[0:2]

                        name = ' '.join(name_list)

                        name = name[1:-1]

                        code_str = ''
                        if name != '':
                            code = main.start(name)

                            for i in code:
                                code_str += i.value + "\n"
                        new_obj.ontimer = Object()
                        new_obj.ontimer.cur = cur_time
                        new_obj.ontimer.tic = tic
                        if name != '':
                            new_obj.ontimer.code = head + code_str + "};"

                    if re.match('oncolor-instr', res.decode('utf-16')) is not None:
                        print("instr")
                        continue;

                    if re.match('oncolor', res.decode('utf-16')) is not None:
                        new_obj.oncolor = []
                        name = res[18:-2].decode('utf-16')
                        print(name)
                        count = 0
                        sub = []
                        for c in name:
                            if c == ' ' and count == 0:
                                continue
                            if c == '[':
                                count += 1
                            elif c == ']':
                                count -= 1

                            sub.append(c)
                            if count == 0:
                                col_obj = Object()
                                tmp_str = ''.join(sub)
                                print(tmp_str)
                                tmp_str = tmp_str[1:-1]
                                str_list = tmp_str.split()
                                col_obj.mode = str_list[0]
                                del str_list[0]
                                tmp_str = ' '.join(str_list)
                                code_str = ''
                                if tmp_str != '':
                                    code = main.start(tmp_str)

                                    for i in code:
                                        code_str += i.value + "\n"

                                if tmp_str != '':
                                    col_obj.code = head + code_str + "};"

                                new_obj.oncolor.append(col_obj)
                                sub = []


                    if re.match('fcn', res.decode('utf-16')) is not None:
                        new_obj.fcn = Object()
                        name = res[10:-2].decode('utf-16')
                        code_str = ''
                        if name != '':
                            code = main.start(name)

                            for i in code:
                                code_str += i.value + "\n"
                        if name != '':
                            new_obj.fcn.code = head + code_str + "};"

                    if re.match('kind', res.decode('utf-16')) is not None:
                        name = res[10:].decode('utf-16')
                        name = int(name)
                        if name == 1:
                            new_obj.fcn.mode = "once"
                        elif name == 0:
                            new_obj.fcn.mode = "forever"

                    if re.match('ontouching', res.decode('utf-16')) is not None:
                        name = res[24:-2].decode('utf-16')
                        code_str = ''
                        if name != '':
                            code = main.start(name)

                            for i in code:
                                code_str += i.value + "\n"
                        if name != '':
                            new_obj.ontouching = head + code_str + "};"

                    if re.match('onmessage', res.decode('utf-16')) is not None:
                        name = res[22:-2].decode('utf-16')
                        code_str = ''
                        if name != '':
                            code = main.start(name)

                            for i in code:
                                code_str += i.value + "\n"
                        if name != '':
                            new_obj.onmessage = head + code_str + "};"


                loc_obj.Object.append(new_obj)

            if loc_cur >= cur:
                break


        obj.pages.append(loc_obj)

    if cur >= file_len:
        break

fo.write(obj.to_JSON())
fo.close()
f.close()
os.remove(f.name)
